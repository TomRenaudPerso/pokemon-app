import {Pokemon} from "../core/domain/entities/pokemon.entity";
import {PokemonId} from "../core/domain/value-objects/pokemon-id.value-object";
import {PokemonBaseExperience} from "../core/domain/value-objects/pokemon-base-experience.value-object";
import {PokemonEvolution} from "../core/domain/entities/pokemon-evolution.entity";
import {PokemonEvolutionError} from "../core/domain/errors/pokemon.error";
import * as pokemonDb from '../mock/pokemons.db.json';

describe('Pokemons tests', () => {
  const pikachuJson = pokemonDb.pokemons.filter(({ name }) => name === 'pikachu')[0];
  const raichuJson = pokemonDb.pokemons.filter(({ name }) => name === 'raichu')[0];
  const pikachuNextEvolutionId = pikachuJson.evolutions[0];
  const pikachuEvolutionJson = pokemonDb.pokemons.filter(({ id }) => id === pikachuNextEvolutionId)[0];
  const pikachu: Pokemon = new Pokemon(
    PokemonId.create(pikachuJson.id),
    pikachuJson.name,
    PokemonBaseExperience.create(pikachuJson.baseExperience),
    pikachuJson.height,
    pikachuJson.weight,
    pikachuEvolutionJson.name
  );
  const raichu: Pokemon = new Pokemon(
    PokemonId.create(raichuJson.id),
    raichuJson.name,
    PokemonBaseExperience.create(raichuJson.baseExperience),
    raichuJson.height,
    raichuJson.weight,
  );

  it('shouldn\'t has a negative id ', () => {
    const pokemonWithNegativeId = () => new Pokemon(
      PokemonId.create(-1),
      pikachuJson.name,
      PokemonBaseExperience.create(pikachuJson.baseExperience),
      pikachuJson.height,
      pikachuJson.weight,
      pikachuEvolutionJson.name
    );
    const expected = new Error('Pokemon id must be greater than 0.');
    expect(pokemonWithNegativeId).toThrow(expected);
  });

  it('should evolve pikachu to raichu', () => {
    const raichu: PokemonEvolution = PokemonEvolution.evolve(pikachu);
    const expected = "raichu";
    expect(raichu.name).toBe(expected);
    expect(raichu.evolutionOf).toBe(pikachu.name);
  });

  it('shouldn\'t evolve raichu', () => {
    const evolutionError = () => PokemonEvolution.evolve(raichu);
    expect(evolutionError).toThrowError(PokemonEvolutionError)
  });
});
