import {ValueObject} from "../../../../common/core/domain/valueObject";

interface PokemonIdProps {
  value: number;
}

export class PokemonId extends ValueObject<PokemonIdProps> {

  get value (): number {
    return this.props.value;
  }

  public static create (id: number) : PokemonId {
    if (id === undefined || id === null || id < 0) {
      throw new Error('Pokemon id must be greater than 0.')
    } else {
      return new PokemonId({ value: id })
    }
  }
}