import {ValueObject} from "../../../../common/core/domain/valueObject";

interface PokemonBaseExperienceProps {
  value: number;
}

export class PokemonBaseExperience extends ValueObject<PokemonBaseExperienceProps> {

  get value (): number {
    return this.props.value;
  }

  public static create (baseExperience: number) : PokemonBaseExperience {
    if (baseExperience === undefined || baseExperience === null || baseExperience < 0) {
      throw new Error('baseExperience must be greater than 0.')
    } else {
      return new PokemonBaseExperience({ value: baseExperience })
    }
  }
}