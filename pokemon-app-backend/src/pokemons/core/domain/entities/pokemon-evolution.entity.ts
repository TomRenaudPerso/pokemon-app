import {Pokemon} from "./pokemon.entity";
import {PokemonId} from "../value-objects/pokemon-id.value-object";
import {PokemonBaseExperience} from "../value-objects/pokemon-base-experience.value-object";
import {PokemonEvolutionError} from "../errors/pokemon.error";
import * as pokemonDb from '../../../mock/pokemons.db.json';

export class PokemonEvolution extends Pokemon {
  private readonly _evolutionOf: string;

  constructor(pokemon: Pokemon, evolutionOf: string) {
    super(
      PokemonId.create(pokemon.id),
      pokemon.name,
      PokemonBaseExperience.create(pokemon.baseExperience),
      pokemon.height,
      pokemon.weight
    );
    this._evolutionOf = evolutionOf;
  }

  get evolutionOf() : string {
    return this._evolutionOf;
  }

  static evolve(pokemon: Pokemon) : PokemonEvolution {
    const pokemonEvolutionJsonFind = pokemonDb.pokemons.find(({ name }) => name === pokemon.nextEvolution)
    if(!pokemonEvolutionJsonFind) {
      throw new PokemonEvolutionError(pokemon.id);
    }
    return new PokemonEvolution(
      new Pokemon(
        PokemonId.create(pokemonEvolutionJsonFind.id),
        pokemonEvolutionJsonFind.name,
        PokemonBaseExperience.create(pokemonEvolutionJsonFind.baseExperience),
        pokemonEvolutionJsonFind.height,
        pokemonEvolutionJsonFind.weight
      ),
      pokemon.name);
  }
}