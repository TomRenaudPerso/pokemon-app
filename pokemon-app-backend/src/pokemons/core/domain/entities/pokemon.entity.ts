import {PokemonId} from "../value-objects/pokemon-id.value-object";
import {PokemonBaseExperience} from "../value-objects/pokemon-base-experience.value-object";

export class Pokemon {
  private readonly _id: PokemonId;
  private readonly _name: string;
  private readonly _baseExperience: PokemonBaseExperience;
  private readonly _height: number;
  private readonly _weight: number;
  private _nextEvolution?: string;

  constructor(
    id: PokemonId,
    name: string,
    baseExperience: PokemonBaseExperience,
    height: number,
    weight: number,
    nextEvolution?: string
  ) {
    this._id = id;
    this._name = name;
    this._baseExperience = baseExperience;
    this._height = height;
    this._weight = weight;
    this._nextEvolution = nextEvolution;
  }

  get id() : number {
    return this._id.value;
  }

  get name() : string {
    return this._name;
  }

  get baseExperience() : number {
    return this._baseExperience.value;
  }

  get height() : number {
    return this._height;
  }

  get weight() : number {
    return this._weight;
  }

  get nextEvolution() : string {
    return this._nextEvolution;
  }

  set nextEvolution(pokemonName: string) {
    this._nextEvolution = pokemonName;
  }
}