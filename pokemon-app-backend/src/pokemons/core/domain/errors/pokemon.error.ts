import {Pokemon} from "../entities/pokemon.entity";

class PokemonError extends Error {
  public pokemonId: Pokemon['id'];
  constructor(pokemonId: Pokemon['id'], message: string) {
    super(message);
    this.message = message;
    this.pokemonId = pokemonId;
  }
}

export class PokemonNotFoundError extends PokemonError {
  constructor(pokemonId: Pokemon['id']) {
    super(pokemonId, `Pokemon with id ${pokemonId} does not exist`);
  }
}

export class PokemonEvolutionError extends PokemonError {
  constructor(pokemonId: Pokemon['id']) {
    super(pokemonId, `Pokemon with id ${pokemonId} has no evolution`);
  }
}